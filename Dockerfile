FROM openjdk:8-alpine

COPY target/uberjar/imagelang.jar /imagelang/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/imagelang/app.jar"]
