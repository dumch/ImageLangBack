(ns imagelang.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [imagelang.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[imagelang started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[imagelang has shut down successfully]=-"))
   :middleware wrap-dev})
