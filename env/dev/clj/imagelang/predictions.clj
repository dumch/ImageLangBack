(ns imagelang.predictions
  (:require [clojure.string :as str]
            [imagelang.routes.events :as events]))

(def repeat->intervals {0 0, 1 1, 2 2, 3 4, 4 8, 5 16, 6 32, 7 64})

; 0 1 2 3
; x x   x
;   x x
;     x x

(defn calc-interactions [days new-words-per-day]
  (loop [days-remains days, acc 0]
    (if (> days-remains 0)
      (let [interactions (loop [d 0, i 1]
                           (if (< d days-remains)
                             (do
                               (prn "d = " d ", i = " i)
                               (recur (+ d (get repeat->intervals (inc i))) (inc i)))
                             i))]
        (recur (dec days-remains)
               (+ acc (* interactions new-words-per-day))))
      acc)))

;;; SELECT SUM(pg_column_size(table_name.*))/COUNT(*) FROM tablename;
;
;(require '[imagelang.db.core :refer [*db*]])
;
;(next.jdbc/execute! *db* ["select (trans,word) from dictionary order by length(trans) desc limit 3"])


