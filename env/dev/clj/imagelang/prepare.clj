(ns imagelang.prepare
  "Common code to prepare db and other resources"
  (:require
    [imagelang.config :refer [env]]
    [imagelang.db.core :as db]
    [next.jdbc :as jdbc]
    [clojure.java.shell :use [sh]]
    [clojure.pprint]
    [dk.ative.docjure.spreadsheet :as sheets]
    [hickory.core :as html]
    [cheshire.core :as j]
    [imagelang.db.core :as db]
    [clojure.string :as str]))

(defn set-up []
  (def files-path (:files-path env))

  #_(def ^:const translations (str files-path "/trans.txt"))

  (def eng (slurp (str files-path "/eng.txt")))

  (def media-map
    (let [raw  (slurp (str files-path "/media"))
          json (j/decode raw)]
      (into {} (map #(hash-map (second %) (first %)) json))))

  (def trans-vec
    (->> (slurp (str files-path "/trans_rus.txt"))
         (str/split-lines)
         (filter #(-> % str/blank? not))
         (partition 3)))

  (def freqs
    (let [words (slurp "resources/prepare/freqs.txt")
          words (str/split-lines words)
          words (map-indexed #(hash-map %2 %1) words)
          words (into {} words)]
      words))
  )

(defn- line->data [idx line]
  (let [strs         (-> line (str/replace #"\"" "") (str/split #"\t"))
        rgx-sound    #"(?<=\[sound:)(.*)(?=\])"
        translations (nth trans-vec idx)
        ]
    {:word               (first strs)
     :trans              (first translations)
     :frequency          (or (get freqs (first strs)) 5000)
     :image_file         (get media-map (-> strs second html/parse html/as-hiccup first (nth 3) (nth 2) second :src))
     :sound_file         (get media-map (first (re-find rgx-sound (nth strs 2))))

     :meaning_sound_file (get media-map (first (re-find rgx-sound (nth strs 3))))
     :meaning_phrase     (nth strs 5)
     :meaning_trans      (second translations)

     :example_sound-file (get media-map (first (re-find rgx-sound (nth strs 4))))
     :example_phrase     (nth strs 6)
     :example_trans      (nth translations 2)
     }))

#_(->> (str/split-lines eng)
       (map-indexed line->data)
       (map #(:word %))
       frequencies
       (sort-by #(second %)))

(defn- fill-db []
  (doseq [word (->> (str/split-lines eng)
                    (map-indexed line->data)
                    (sort-by #(:frequency %)))]
    (db/create-word! word)))

;; working with db
(def opts {:builder-fn next.jdbc.result-set/as-unqualified-maps})

(comment
  (jdbc/execute! db/*db* ["select * from dictionary tablesample system (2)"])
  (jdbc/execute! db/*db* ["select (word, trans, meaning_phrase) from dictionary where word = 'gulf'"])
  (jdbc/execute! db/*db* ["UPDATE dictionary SET trans = 'пропасть' where word = 'gulf'"]))


;; fix all the translations

(defn remove-from-end [s end]
  (if (.endsWith s end)
      (.substring s 0 (- (count s)
                         (count end)))
    s))

;; clean phrases
#_(filter #(.endsWith (nth % 3) ".") (for [word (db/get-words {:count 30})
      :let [id (:id word)
            fix-f          #(remove-from-end % ".")
            meaning_phrase (fix-f (:meaning_phrase word))
            example_phrase (fix-f (:example_phrase word))
            meaning_trans  (fix-f (:meaning_trans word))
            example_trans  (fix-f (:example_trans word))]]

  [meaning_trans meaning_phrase example_trans example_phrase]
  #_(next.jdbc.sql/update!
   db/*db*
   :dictionary 
   {:meaning_trans meaning_trans
    :example_trans example_trans
    :meaning_phrase meaning_phrase
    :example_phrase example_phrase} 
   {:id id} 
  )))

;; exel

(defn db->exel []
  (let [wb         (sheets/create-workbook
                     "Words and context"
                     (cons
                       ["id" "Word" "Translation" "Meaning" "MTranslation" "Example" "ETranslation"]
                       (map #(vector (:id %)
                                     (:word %)
                                     (:trans %)
                                     (:meaning_phrase %)
                                     (:meaning_trans %)
                                     (:example_phrase %)
                                     (:example_trans %))
                            (jdbc/execute! db/*db* ["select * from dictionary"] opts))))
        sheet      (sheets/select-sheet "Words and context" wb)
        header-row (first (sheets/row-seq sheet))]
    (sheets/set-row-style! header-row (sheets/create-cell-style! wb {:background :yellow,
                                                                     :font       {:bold true}}))
    (sheets/save-workbook! "resources/public/words.xlsx" wb)))


