(ns imagelang.prototyping
  (:import (java.util Date)))

;; events
{:meta {:input  [{:type "button" :text "кошка" :properties ["exercise" "native"] :action [{:sound "url"} {:exercise "success"}]}
                 {:type "button" :text "собака" :properties ["exercise" "native"] :action [{:exercise "fail"}]}
                 {:type "button" :text "окно" :properties ["exercise" "native"] :action [{:exercise "fail"}]}
                 {:type "button" :text "не знаю" :properties ["native"] :action [{:show-card 4}]}
                 {:type "button" :action [{:sound "url"}]}
                 {:type "text" :text "cat" :properties ["learning"]}
                 {:type "text" :text "Cat is sleeping" :properties ["learning"]}
                 {:type "image" :image "http://image.url.com"}]
        :action [{:sound "url"}]
        :happen ["success" "next"]
        }}

{:meta {:input    [{:button "кошка" :selected false :lang "native"}
                   {:button "собака" :selected false :lang "native"}
                   {:button "окно" :selected false :lang "native"}
                   {:text "cat" :lang "learning"}
                   {:text "Cat is sleeping" :lang "learning"}
                   {:image "http://image.url.com"}
                   {:sound "http://some.sound.com"}]
        :type     "exercise"                                ;; result, card
        :chosen   "кошка"
        :expected "кошка"}}


{:word_id 1, :user_id "1", :repeat_index 0, :interaction_count 3, :event_date (Date.)
 :meta    {:input  {:type  "exercise"
                    :items {:images     ["https://image.url.jpg"]
                            :learn_word ["cat"]
                            :learn_text ["I like cats"]}}
           :action {:type   "variants"
                    :values ["кошка" "собака" "слон"]
                    :result "success" #_("fail" "next" "previous")}
           }}