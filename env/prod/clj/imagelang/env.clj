(ns imagelang.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[imagelang started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[imagelang has shut down successfully]=-"))
   :middleware identity})
