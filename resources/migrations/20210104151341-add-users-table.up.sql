CREATE TABLE users
(id         VARCHAR(20) PRIMARY KEY,
 device_id  VARCHAR(255),
 first_name VARCHAR(30),
 last_name  VARCHAR(30),
 email      VARCHAR(30),
 admin      BOOLEAN,
 mode       SMALLINT DEFAULT 0,
 last_login TIMESTAMP,
 is_active  BOOLEAN,
 pass       VARCHAR(300));
