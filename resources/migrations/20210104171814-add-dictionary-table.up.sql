CREATE TABLE dictionary
 (id                 SERIAL PRIMARY KEY,
  learning_lang      VARCHAR(3)  NOT NULL DEFAULT 'eng',
  native_lang        VARCHAR(3)  NOT NULL DEFAULT 'rus',
  word               VARCHAR(20) NOT NULL,
  trans              VARCHAR(30) NOT NULL,
  frequency          INTEGER,
  image_file         VARCHAR(21) NOT NULL,
  sound_file         VARCHAR(21) NOT NULL,

  meaning_phrase     TEXT        NOT NULL,
  meaning_trans      TEXT        NOT NULL,
  meaning_sound_file VARCHAR(21) NOT NULL,

  example_phrase     TEXT        NOT NULL,
  example_trans      TEXT        NOT NULL,
  example_sound_file VARCHAR(21) NOT NULL
);
--;;
CREATE TABLE events
 (id                 BIGSERIAL   PRIMARY KEY,
  word_id            INTEGER     NOT NULL,
  user_id            VARCHAR(20) NOT NULL,
  repeat_index       SMALLINT,
  interaction_count  SMALLINT,
  event_date         TIMESTAMP,
  meta               JSONB,
  FOREIGN KEY (word_id) REFERENCES dictionary(id) ON DELETE CASCADE,
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);
--;;
CREATE INDEX events_word_ids ON events USING btree(word_id);
--;;
CREATE INDEX events_user_ids ON events USING btree(user_id);
--;;
CREATE INDEX ids             ON events USING btree(user_id, word_id);
