-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(id, first_name, last_name, email, pass)
VALUES (:id, :first_name, :last_name, :email, :pass)

-- :name update-user! :! :n
-- :doc updates an existing user record
UPDATE users SET
--~ (if (contains? params :pass) "pass = :pass" "pass = pass")
--~ (when (contains? params :first_name) ",first_name = :first_name")
--~ (when (contains? params :first_name) ",last_name = :last_name")
--~ (when (contains? params :email) ",email = :email")
--~ (when (contains? params :last_login) ",last_login = :last_login")
--~ (when (contains? params :features) ",features = :features")
WHERE id = :id


-- :name get-user :? :1
-- :doc retrieves a user record given the id
SELECT * FROM users
WHERE id = :id

-- :name delete-user! :! :n
-- :doc deletes a user record given the id
DELETE FROM users
WHERE id = :id


-- :name create-word! :! :n
-- :doc add a new word record
INSERT INTO dictionary
(word, trans, frequency, image_file, sound_file,
 meaning_phrase, meaning_trans, meaning_sound_file,
 example_phrase, example_trans, example_sound_file)
VALUES
(:word, :trans, :frequency, :image-file, :sound-file,
 :meaning-phrase, :meaning-trans, :meaning-sound-file,
 :example-phrase, :example-trans, :example-sound-file)

-- :name get-words :? :*
-- :doc retrieves a word record given word
SELECT * FROM dictionary
--~(when (contains? params :word) "WHERE word = :word")
LIMIT
--~(if (contains? params :count) ":count" "1")

-- :name get-word :? :1
-- :doc retrieves a word record given word
SELECT * FROM dictionary
WHERE id = :id

-- :name get-words-to-study :? :*
-- :doc retrieves words that user never learn given user_id
SELECT * FROM dictionary d
WHERE NOT EXISTS (
   SELECT DISTINCT word_id
   FROM   events
   WHERE  user_id = :user_id AND word_id = d.id
)
ORDER BY d.frequency
LIMIT
--~(if (contains? params :count) ":count" "10")

-- :name get-words-to-repeat :? :*
-- :doc retrieves words with expired repetition interval given user_id
WITH T AS (
  SELECT word_id, MAX(event_date) AS latest_date
  FROM events
  WHERE user_id = :user_id
  GROUP BY word_id
), T2 AS (
  SELECT events.word_id FROM events
  JOIN T ON T.word_id = events.word_id AND
            T.latest_date = events.event_date AND
            events.user_id = :user_id AND
            (CASE repeat_index WHEN 0 THEN 0
                               WHEN 1 THEN 1
                               WHEN 2 THEN 2
                               WHEN 3 THEN 4
                               WHEN 4 THEN 8
                               WHEN 5 THEN 16
                               WHEN 6 THEN 32
                               ELSE 64
            END) <= DATE_PART('day', now()::timestamp - events.event_date::timestamp)
)
SELECT dictionary.* FROM dictionary
JOIN T2 ON T2.word_id = dictionary.id
LIMIT
--~(if (contains? params :count) ":count" "10")

-- :name get-extra-words :? :*
-- :doc retrieves words with most diff between interaction and repetition given user_id
WITH T AS (
   SELECT *,
   ROW_NUMBER() OVER(
        PARTITION BY word_id
        ORDER     BY event_date DESC
   ) AS rn
   FROM events WHERE user_id = :user_id
)
SELECT dictionary.* FROM dictionary
JOIN T ON dictionary.id = T.word_id AND T.rn = 1
ORDER BY (interaction_count - repeat_index) DESC
LIMIT
--~(if (contains? params :count) ":count" "10")


-- :name get-dictionary-sample :? :*
-- :doc retrieves random words from dictionary table
SELECT * FROM dictionary TABLESAMPLE SYSTEM (4)
LIMIT
--~(if (contains? params :count) ":count" "20")

-- :name get-recent-events :? :*
-- :doc retrieves most recent events for each word given user-id
WITH T AS (
  SELECT word_id, MAX(event_date) AS latest_date
  FROM events
  WHERE user_id = :user_id
  GROUP BY word_id
)
SELECT events.* FROM events
JOIN T ON T.word_id = events.word_id AND
          T.latest_date = events.event_date AND
          events.user_id = :user_id

-- :name get-recent-event :? :1
-- :doc retrieves most recent event given user_id and word_id
SELECT * FROM events WHERE
      event_date = (SELECT MAX(event_date) FROM events
                    WHERE user_id = :user_id AND
                          word_id = :word_id)
LIMIT 1

-- :name get-learned-today-count :? :1
-- :doc retrieves today learned words given user_id
SELECT count(DISTINCT word_id) FROM events e
WHERE  user_id = :user_id
AND    meta->>'result' = 'SUCCESS'
AND    NOT EXISTS(
   SELECT DISTINCT word_id FROM events
   WHERE user_id = :user_id
   AND   word_id = e.word_id
   AND   DATE_PART('day', now()::timestamp - events.event_date::timestamp) > 0
)

-- :name get-learned-count :? :1
-- :doc retrieves all learned words given user_id
SELECT count(DISTINCT word_id) FROM events
WHERE  user_id = :user_id
AND    meta->>'result' = 'SUCCESS'
