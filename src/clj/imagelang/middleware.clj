(ns imagelang.middleware
  (:require
    [imagelang.env :refer [defaults]]
    [imagelang.config :refer [env]]
    [ring.middleware.flash :refer [wrap-flash]]
    [ring.adapter.undertow.middleware.session :refer [wrap-session]]
    [ring.middleware.session.cookie :refer [cookie-store]]
    [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
    [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
    [buddy.auth.accessrules :refer [restrict]]
    [buddy.auth :refer [authenticated?]]
    [buddy.auth.backends.session :refer [session-backend]]))

(defn on-error [request response]
  {:status  403
   :headers {}
   :body    (str "Access to " (:uri request) " is not authorized")})

(defn wrap-restricted [handler]
  (restrict handler {:handler  authenticated?
                     :on-error on-error}))

(defn wrap-auth [handler]
  (let [backend (session-backend)]
    (-> handler
        (wrap-authentication backend)
        (wrap-authorization backend))))

#_(defn- valid-secret-key? [key]
    (and (= (type (byte-array 0)) (type key))
         (= (count key) 16)))

#_(valid-secret-key? (key->bytes (:session-store-key env)))

#_(defn wrap-cors
  "Wrap the server response in a Control-Allow-Origin Header to
  allow connections from the web app."
  [handler]
  (fn [request]
    (let [response (handler request)]
      (-> response
          (assoc-in [:headers "Access-Control-Allow-Origin"] "*")
          (assoc-in [:headers "Access-Control-Allow-Credentials"] "true")
          (assoc-in [:headers "Access-Control-Allow-Headers"] "*")
          (assoc-in [:headers "Access-Control-Expose-Headers"] "Authorization, authenticated")
          (assoc-in [:headers "Access-Control-Allow-Methods"] "*")))))

(defn wrap-base [handler]
  (-> ((:middleware defaults) handler)
      wrap-auth
      wrap-flash
      ; wrap-cors
      ; cookie with the HttpOnly attribute is inaccessible to the JavaScript Document.cookie API
      #_(wrap-session {:cookie-attrs {:http-only true}})
      (wrap-defaults
        (-> site-defaults
            (assoc-in [:security :anti-forgery] false)
            ;(assoc-in [:session :cookie-attrs :same-site] :lax) ;; for oauth
            (assoc-in [:session :store] (cookie-store {:key (.getBytes (:session-store-key env))}))
            #_(dissoc :session)))))
