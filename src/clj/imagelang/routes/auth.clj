(ns imagelang.routes.auth
  (:require [imagelang.db.core :as db :refer [*db*]]
            [imagelang.util.validation :as validate]
            [imagelang.util.responses :as resp]
            [imagelang.util.utils :as utils]
            [imagelang.util.resources :as res]
            [clojure.tools.logging :as log]
            [ring.util.http-response :as ring-resp]
            [buddy.hashers :as hashers]
            [clojure.pprint :refer [pprint]])
  (:import (java.util Base64)
           (java.nio.charset Charset)
           (java.time LocalDateTime)))

(defn- handle-reg-exc [e lang]
  (let [duplicate? (->> (utils/ex-chain e)
                        (map ex-message)
                        (filter #(.startsWith % "ERROR: duplicate key"))
                        seq)]
    (if duplicate?
      (resp/e-precondition (res/str-get :user-already-exist lang))
      (resp/e-internal e (res/str-get :server-error lang)))))

(defn- enrich-session [response session {id :id admin :admin}]
  (assoc response :session
                  (assoc session :identity id, :roles (when admin #{:admin}))))

(defn register! [{session :session :as req} user lang]
  (if-let [errors (validate/registration-errors user lang)]
    (resp/e-precondition (res/str-get :validation-fail lang) {:validation errors})
    (try
      (db/create-user! (-> user
                           (assoc :email nil)
                           (assoc :first_name nil)
                           (assoc :last_name nil)
                           (dissoc :pass_confirm)
                           (update :pass hashers/encrypt)))
      (-> {:result :ok}
          (ring-resp/ok)
          (enrich-session session user))
      (catch Exception e (handle-reg-exc e lang)))))

;(next.jdbc/execute! *db* ["select * from users"])

#_(str "Basic " (.encodeToString (Base64/getEncoder) (.getBytes "dumch:1Q2w3edu")))

(defn- decode-auth [encoded]
  (let [auth (second (.split encoded " "))]
    (-> (.decode (Base64/getDecoder) auth)
        (String. (Charset/forName "UTF-8"))
        (.split ":"))))

(defn update-last-login!
  "non blocking; logs on error"
  [id]
  (future
    (try
      (db/update-user! {:id id :last_login (LocalDateTime/now)})
      (catch Exception e
        (log/error "can't update last_login for id" id e)))))

(defn- authenticate!
  "returns validation and user"
  [[id pass] lang]
  (if-let [user (db/get-user {:id id})]
    (if (hashers/check pass (:pass user))
      (do (update-last-login! id)
          [nil user])
      [{:pass (res/str-get :incorrect-password lang)} nil])
    [{:id (res/str-get :user-does-not-exist lang)} nil]))

(defn login! [{:keys [session] :as req} auth lang]
  (let [[validation user] (authenticate! (decode-auth auth) lang)]
    (if user
      (-> {:result :ok}
          (ring-resp/ok)
          (enrich-session session user))
      (ring-resp/unauthorized {:result     :fail
                               :validation validation
                               :message    (res/str-get :login-fail lang)}))))

(defn logout! [req]
  (pprint req)
  (-> {:result :ok}
      (ring-resp/ok)
      (assoc :session nil)))

(defn change-pass! [id new-pass lang]
  (if-let [errors (validate/change-pass-errors new-pass lang)]
    (resp/e-precondition (res/str-get :validation-fail lang) {:validation errors})
    (if-let [_ (db/get-user {:id id})]
      (resp/wrap-internal-error
        :fun (fn []
               (db/update-user! {:id id :pass (hashers/encrypt new-pass)})
               {:result :ok})
        :msg (res/str-get :server-error lang))
      (ring-resp/unauthorized {:result  :unauthorized
                               :message "user does not exist"}))))

(defn delete-account! [identity]
  (db/delete-user! {:id identity})
  (-> {:result :ok}
      (ring-resp/ok)
      (assoc :session nil)))
