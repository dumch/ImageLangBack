(ns imagelang.routes.events
  (:require [imagelang.db.core :as db :refer [*db*]]
            [imagelang.util.specs :as specs]
            [imagelang.config :refer [env]]
            [imagelang.util.resources :as res]
            [imagelang.util.utils :refer :all]
            [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [ring.util.http-response :as resp]
            [imagelang.util.responses :refer [wrap-internal-error]]
            [clojure.tools.logging :as log]
            [clojure.spec.alpha :as s :refer [valid?]]
            [clojure.spec.gen.alpha :as gen])
  (:import (java.util Date)
           (java.time LocalDateTime)
           (java.time.temporal ChronoUnit)
           (java.sql Timestamp)))

(def ^:const SUCCESS "SUCCESS")
(def ^:const FAIL "FAIL")
(def ^:const NEXT "NEXT")
(def ^:const PREVIOUS "PREVIOUS")

(def ^:const CARD "CARD")
(def ^:const EXERCISE "EXERCISE")
(def ^:const RESULT "RESULT")

(def ^:const WORD_TR "WORD_TR")
(def ^:const TR_WORD "TR_WORD")
(def ^:const PROCEED "PROCEED")
(def ^:const WRITE "WRITE")

(s/def ::user_id string?)
(s/def ::word_id :imagelang.routes.words/id)
(s/def ::repeat_index (s/int-in 0 15))
(s/def ::interaction_count (s/int-in 0 (dec (Math/pow 2 15))))
(s/def ::event_date inst?)

(s/def ::action_type #{WORD_TR TR_WORD PROCEED WRITE})
(s/def ::input_type #{CARD EXERCISE RESULT})
(s/def ::result #{SUCCESS FAIL NEXT PREVIOUS})
(s/def ::meta (s/keys :req-un [::input_type ::action_type ::result]))

(s/def ::event
  (s/keys :req-un [::word_id ::user_id ::repeat_index ::interaction_count ::event_date ::meta]))

(s/def :user/event
  (s/keys :req-un [::word_id ::meta]
          :opt-un [::event_date]))

(defn- date->local-date-time [date]
  (cond (nil? date) (LocalDateTime/now)
        (instance? Date date) (.toLocalDateTime (Timestamp. (.getTime date)))
        (instance? LocalDateTime date) date
        :else (LocalDateTime/parse (str date))))

(defn- enrich
  "last-event could be nil"
  [user-id event last-event]
  (let [meta      (:meta event)
        exercise? (-> meta :input_type (= EXERCISE))
        now       (date->local-date-time (:event_date event))
        ;_         (prn "last event " (dissoc last-event :meta))
        previous  (if last-event (date->local-date-time (:event_date last-event)) now)
        ;_         (prn "previous " previous)
        days-diff (.until now previous (ChronoUnit/DAYS))
        success?  (when exercise? (= (-> meta :result) SUCCESS))
        fail?     (when exercise? (= (-> meta :result) FAIL))
        rep-idx-f (cond (= 0 days-diff) identity            ;; first day fails and success do not mean
                        fail? dec
                        success? inc
                        :else identity)
        rep-idx   (rep-idx-f (or (:repeat_index last-event) 1))]
    (assoc event
      :repeat_index rep-idx
      :interaction_count (inc (or (:interaction_count last-event) 0))
      :user_id user-id
      :event_date now)))

(defn on-event [ds user-id event]
  (wrap-internal-error
    :fun #(let [word-id      (:word_id event)
                recent-event (db/get-recent-event ds {:user_id user-id, :word_id word-id})
                ;_            (prn "recent event is " (dissoc recent-event :meta))
                event+       (enrich user-id event recent-event)
                ;_            (prn "new event is " (dissoc event+ :meta))
                ]
            (sql/insert! ds :events event+)
            {:result :ok})
    :msg "can't store event"))

(defn- enrich-events
  "param events — with the same word_id;
  param predecessor - previous event;
  return enriched events"
  [user-id predecessor events]
  (loop [prev   predecessor
         events (sort-by :event_date events)
         result []]
    (if (empty? (seq events))
      result
      (let [event+ (enrich user-id (first events) prev)]
        (recur event+ (rest events) (conj result event+))))))

(defn on-events [ds user-id events]
  (wrap-internal-error
    :fun #(do
            (doseq
              [events-by-word-id (->> events (group-by :word_id) (map second))
               :let [word-id (-> events-by-word-id first :word_id)
                     prev    (db/get-recent-event ds {:user_id user-id, :word_id word-id})
                     events+ (enrich-events user-id prev events-by-word-id)]]
              (doseq [e+ events+] (sql/insert! ds :events e+)))
            {:result :ok})
    :msg "can-t store events"))

(defn progress [ds user-id lang]
  (if user-id
    (wrap-internal-error
      :fun (fn [] {:studied {:today (:count (db/get-learned-today-count ds {:user_id user-id}))
                             :all   (:count (db/get-learned-count ds {:user_id user-id}))}})
      :msg (res/str-get :server-error lang))
    (resp/unauthorized {:result :fail, :message (res/str-get :login-fail lang)})))

;; examples
(comment
  (s/explain ::event                                        ; card to study new word
             {:word_id           1, :user_id "1"
              :repeat_index      0
              :interaction_count 3                          ; interaction count
              :event_date        (Date.)
              :meta              {:input_type  "CARD"
                                  :action_type "PROCEED"
                                  :result      "NEXT"}})

  (s/explain ::event                                        ; word-translation exercise
             {:word_id 1, :user_id "1", :repeat_index 0, :interaction_count 3, :event_date (Date.)
              :meta    {:input_type  "EXERCISE"
                        :action_type "WORD_TR"
                        :result      "SUCCESS"}})

  (s/explain ::event                                        ; word-translation result
             {:word_id 1, :user_id "1", :repeat_index 0, :interaction_count 3, :event_date (Date.)
              :meta    {:input_type  "RESULT"
                        :action_type "PROCEED"
                        :result      "NEXT"}})

  (s/explain ::event                                        ; image-word exercise
             {:word_id 2, :user_id "1", :repeat_index 0, :interaction_count 4, :event_date (Date.)
              :meta    {:input_type  "EXERCISE"
                        :action_type "WRITE"
                        :result      "SUCCESS"}}))

(comment
  (require '[clojure.data.json :as json])
  (require '[robot.core :as robot])

  (robot/clipboard-put!
    (json/write-str
      {:word_id 2,
       :meta    {:input  {:type  EXERCISE
                          :items {:images     ["https://image.url.jpg", "https://image.url2.jpg"]
                                  :learn_text ["The ... is chasing the mouse"]}}
                 :action {:type   WRITE
                          :values []
                          :result SUCCESS}
                 :chosen ["cat"]}}))


  (jdbc/execute! *db* ["select * from events"])

  (db/create-user!
    *db*
    {:id         "test"
     :first_name "Sam"
     :last_name  "Smith"
     :email      "sam.smith@example.com"
     :pass       "pass"}
    )

  (jdbc/execute! *db* ["select count(*) from dictionary"])

  )
