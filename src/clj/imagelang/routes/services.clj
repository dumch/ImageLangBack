(ns imagelang.routes.services
  (:require
    [reitit.swagger :as swagger]
    [reitit.swagger-ui :as swagger-ui]
    [reitit.ring.coercion :as coercion]
    [reitit.coercion.spec :as spec-coercion]
    [reitit.ring.middleware.muuntaja :as muuntaja]
    [reitit.ring.middleware.multipart :as multipart]
    [reitit.ring.middleware.parameters :as parameters]
    [imagelang.middleware.formats :as formats]
    [ring.util.http-response :refer :all]
    [clojure.tools.logging :as log]
    [imagelang.routes.words :as words]
    [imagelang.routes.events :as events]
    [imagelang.routes.settings :as settings]
    [imagelang.config :refer [env]]
    [imagelang.routes.auth :as auth]
    [imagelang.db.core :refer [*db*]]
    [imagelang.util.validation :as validation]
    [imagelang.middleware :as middleware]
    [clojure.spec.alpha :as s]
    [clojure.string :as str]))

(defn service-routes []
  ["/api"
   {:coercion   spec-coercion/coercion
    :muuntaja   formats/instance
    :swagger    {:id ::api}
    :middleware [parameters/parameters-middleware
                 muuntaja/format-negotiate-middleware
                 muuntaja/format-response-middleware
                 coercion/coerce-exceptions-middleware
                 muuntaja/format-request-middleware
                 coercion/coerce-response-middleware
                 coercion/coerce-request-middleware
                 multipart/multipart-middleware]}

   ;; swagger documentation
   ["" {:no-doc  true
        :swagger {:info {:title       "my-api"
                         :description "https://cljdoc.org/d/metosin/reitit"}}}

    ["/swagger.json"
     {:get (swagger/create-swagger-handler)}]

    ["/api-docs/*"
     {:get (swagger-ui/create-swagger-ui-handler
             {:url    "/api/swagger.json"
              :config {:validator-url nil}})}]]

   ["/ping"
    {:get (constantly (ok {:message "pong"}))}]

   ["/study"
    {:swagger {:tags ["study"]}}
    ["/event"
     {:post {:summary    "send what has happened"
             :parameters {:body :user/event}
             :responses  {200 {:body any?}}
             :handler    (fn [{{id :identity} :session :as ses {event :body} :parameters}]
                           (events/on-event *db* id event))}}]

    ["/events"
     {:post {:summary    "send batch of events"
             :parameters {:body (s/coll-of :user/event)}
             :responses  {200 {:body {:result keyword?}}}
             :handler    (fn [{{id :identity} :session :as ses {events :body} :parameters}]
                           (events/on-events *db* id events))}}]

    ["/mode"
     {:put {:summary    "set up mode type: difficulty"
            :parameters {:query {:mode (s/int-in 0 2)}}
            :responses  {200 {:body any?}}
            :handler    (fn [{{id :identity} :session :as ses {{mode :mode} :query} :parameters}]
                          (settings/set-mode id mode :ru-RU))}

      :get {:summary    "get mode type: difficulty"
            :responses  {200 {:body {:mode (s/int-in 0 2)}}}
            :handler    (fn [{{id :identity} :session :as ses}]
                          (settings/get-mode id :ru-RU))}}]

    ["/extra-words"
     {:middleware [middleware/wrap-restricted]
      :get        {:summary   "get extra-words to repeat for current user"
                   :responses {200 {:body {:words             (s/coll-of :exercise/word),
                                           :dictionary        (s/coll-of ::words/word)
                                           :dictionary_native (s/coll-of ::words/word)}}}
                   :handler   (fn [{{id :identity} :session}]
                                (words/get-extra-words-resp (if (str/blank? id) nil id) :ru-RU))}}]

    ["/words"
     {:get {:summary   "get words to learn for current user"
            :responses {200 {:body {:words             (s/coll-of :exercise/word),
                                    :dictionary        (s/coll-of ::words/word)
                                    :dictionary_native (s/coll-of ::words/word)}}}
            :handler   (fn [{{id :identity} :session}]
                         (words/get-words-resp (if (str/blank? id) nil id) :ru-RU))}}]

    ["/random-words"
     {:get {:summary    "get random words"
            :parameters {:query {:count (s/int-in 1 100)}}
            :responses  {200 {:body {:words             (s/coll-of :exercise/word),
                                     :dictionary        (s/coll-of ::words/word)
                                     :dictionary_native (s/coll-of ::words/word)}}}
            :handler    (fn [{{{c :count} :query} :parameters}]
                          (words/get-random-words-resp c :ru-RU))}}]

    ["/progress"
     {:get {:summary   "get user progress: all and today"
            :responses {200 {:body {:studied {:today nat-int? :all nat-int?}}}}
            :handler   (fn [{{id :identity} :session}]
                         (events/progress *db* id :ru-RU))}}]]

   ["/auth"
    {:swagger {:tags ["auth"]}}
    ["/register"
     {:post {:summary    "register a user, providing id and password"
             :parameters {:body {:id           string?
                                 :pass         string?
                                 :pass_confirm string?}}
             :responses  {200 {:body {:result keyword?}}}
             :handler    (fn [{{user :body} :parameters :as req}]
                           (auth/register! req user :ru-RU))}}]

    ["/login"
     {:post {:summary    "login a user and create a session"
             :parameters {:header {:authorization string?}}
             :responses  {200 {:body {:result keyword?}}}
             :handler    (fn [{{{h :authorization} :header} :parameters :as req}]
                           (auth/login! req h :ru-RU))}}]

    ["/logout"
     {:post {:summary   "remove user session"
             :responses {200 {:body {:result keyword?}}}
             :handler   (fn [req] (auth/logout! req))}}]]

   ["/restricted"
    {:swagger    {:tags ["restricted"]}
     :middleware [middleware/wrap-restricted]}

    ["/change-pass"
     {:post {:summary    "change user's password"
             :parameters {:body {:pass string?}}
             :responses  {200 {:body {:result keyword?}}}
             :handler    (fn [{{id :identity :as s} :session
                               {{p :pass} :body}    :parameters :as req}]
                           (clojure.pprint/pprint {:session s})
                           (auth/change-pass! id p :ru-RU))}}]

    ["/delete-account"
     {:post {:summary   "delete user profile from database"
             :responses {200 {:body {:result keyword?}}}
             :handler   (fn [{{id :identity} :session}] (auth/delete-account! id))}}]]])
