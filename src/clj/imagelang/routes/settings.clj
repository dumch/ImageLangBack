(ns imagelang.routes.settings
  (:require [clojure.pprint :refer [pprint]]
            [clojure.tools.logging :as log]
            [imagelang.db.core :as db :refer [*db*]]
            [imagelang.util.resources :as res]
            [imagelang.util.responses :as resp]
            [imagelang.util.utils :as utils]
            [next.jdbc.sql :as sql]
            [ring.util.http-response :as ring-resp]))

(def id->count {0 10, 1 20})

(defn words-count [{mode :mode}]
  (get id->count mode))

(defn set-mode [id mode-id lang]
  (resp/wrap-internal-error
    :fun #(sql/update! *db* :users {:mode mode-id} {:id id})
    :msg (res/str-get :server-error lang)))

(defn get-mode [id lang]
  (resp/wrap-internal-error 
    :fun #(hash-map :mode (:users/mode (first (sql/query *db* ["SELECT (mode) FROM users WHERE id = ?" id]))))
    :msg (res/str-get :server-error lang)))