(ns imagelang.routes.words
  (:require [imagelang.db.core :as db :refer [*db*]]
            [imagelang.util.responses :refer [wrap-internal-error]]
            [imagelang.util.specs :refer :all]
            [imagelang.util.resources :as res]
            [imagelang.util.utils :as utils]
            [imagelang.routes.settings :as settings]
            [clojure.spec.alpha :as s :refer [valid?]]
            [clojure.spec.gen.alpha :as gen]
            [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [ring.util.http-response :as ring-resp]
            [clojure.tools.logging :as log]
            [imagelang.config :refer [env]]
            [cuerdas.core :as str]))

;; SPECS
(s/def ::id nat-int?)
(s/def ::learning_lang (str-with-len>? 3))
(s/def ::native_lang (str-with-len>? 3))
(s/def ::word (str-with-len>? 20))
(s/def ::trans (str-with-len>? 29))
(s/def ::frequency pos-int?)
(s/def ::meaning_phrase string?)
(s/def ::meaning_trans string?)
(s/def ::example_phrase string?)
(s/def ::example_trans string?)

(s/def ::image_file string?)
(s/def ::sound_file string?)
(s/def ::meaning_sound_file string?)
(s/def ::example_sound_file string?)

(s/def :db/image_file (str-with-len>? 21))
(s/def :db/sound_file (str-with-len>? 21))
(s/def :db/meaning_sound_file (str-with-len>? 21))
(s/def :db/example_sound_file (str-with-len>? 21))

;; what we get from database
(s/def ::dictionary-row
  (s/keys :req-un [::id ::learning_lang ::native_lang ::word ::trans
                   ::frequency :db/image_file :db/sound_file
                   ::meaning_phrase ::meaning_trans :db/meaning_sound_file
                   ::example_phrase ::example_trans :db/example_sound_file]))

(def ^:const STUDY_CARD "STUDY_CARD")
(def ^:const WORD_TRANSLATION "WORD_TRANSLATION")
(def ^:const TRANSLATION_WORD "TRANSLATION_WORD")

(def types #{STUDY_CARD WORD_TRANSLATION TRANSLATION_WORD})
(def exercise-types (vec (disj types STUDY_CARD)))

(s/def :exercise/type types)
(s/def :exercise/types (s/coll-of :exercise/type))

;; what user gets from rest
(s/def :exercise/word (s/keys :req-un [::id ::learning_lang ::native_lang ::word ::trans
                                       ::frequency ::image_file ::sound_file
                                       :exercise/types
                                       ::meaning_phrase ::meaning_trans ::meaning_sound_file
                                       ::example_phrase ::example_trans ::example_sound_file]))

;; IMPL

(defn- fix-links [word]
  (let [path     (get env :files-path)
        fix-path #(str path "/" %)
        upd      (fn [m k] (update m k fix-path))]
    (reduce upd word [:image_file :sound_file :example_sound_file :meaning_sound_file])))

(defn- +exercises-types [word]
  (assoc word :types (vector (rand-nth exercise-types))))

(defn- +dictionary
  ([dict-count words]
   (+dictionary dict-count #(db/get-dictionary-sample *db* %) words))

  ([dict-count get-dic-sample words]
   (let [c           (if (empty? words) 0 (* 2 dict-count))
         dict        (get-dic-sample {:count c})
         dict-native (->> dict (take (/ c 2)) (map :trans))
         dict-learn  (->> dict (drop (/ c 2)) (map :word))]
     {:words words, :dictionary dict-learn, :dictionary_native dict-native})))

(defn- get-words-to-learn-and-repeat
  ([id words-count]
   "provide words to learn and repeat from database"
   (get-words-to-learn-and-repeat id words-count db/get-words-to-repeat db/get-learned-today-count db/get-words-to-study))

  ([id words-count get-words-to-repeat get-studied-today-count get-to-study]
   "provide words to learn and repeat given functions to extract words from any source "
   (let [user-info       {:user_id id}
         words-to-repeat (map +exercises-types (get-words-to-repeat user-info))
         remains-count   (max 0 (- words-count (-> user-info get-studied-today-count :count)))
         words-to-learn  (->> (get-to-study {:user_id id, :count remains-count})
                              (map #(assoc % :types types)))]
     (->> (concat words-to-repeat words-to-learn)
          (map fix-links)))))

(defn- get-words-count [id]
  (let [user (db/get-user {:id id})
        words-count (settings/words-count user)
        dict-count  (* 3 words-count)]
    [words-count dict-count]))

(defn get-random-words-resp [cnt lang]
  (wrap-internal-error
    :fun #(->> (get-words-to-learn-and-repeat nil cnt)
               (+dictionary (* 3 cnt)))
    :msg (res/str-get :server-error lang)))

(defn get-words-resp [id lang]
  (if id
    (wrap-internal-error
     :fun #(let [[words-count, dict-count] (get-words-count id)]
             (->> (get-words-to-learn-and-repeat id words-count)
                  (+dictionary dict-count)))
     :msg (res/str-get :server-error lang))
    (ring-resp/unauthorized)))

(defn get-extra-words-resp [id lang]
  (wrap-internal-error
    :fun #(let [[words-count, dict-count] (get-words-count id)]
            (->> (db/get-extra-words *db* {:user_id id, :count words-count})
                 (map +exercises-types)
                 (map fix-links)
                 (+dictionary dict-count)))
    :msg (res/str-get :server-error lang)))
