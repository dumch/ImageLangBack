(ns imagelang.util.resources)

(def ^:const rus "ru-RU")

(def ^:private dictionary
  {:server-error             "что-то пошло не так"
   :validation-fail          "ошибка валидации"
   :login-fail               "ошибка аутентификации"
   :login-validation         "логин должен содержать от 2 до 20 латинских символов"
   :password-validation      "пароль должен содержать от 6 до 20 символов, включая хотя бы 1 латинский символ и 1 цифру"
   :pass-confirm-validation  "пароли не совпадают"
   :unique-person-validation "невалидный юзер"
   :incorrect-password       "неверный пароль"
   :user-does-not-exist      "юзера с таким ID не существует"
   :user-already-exist       "юзер с таким ID уже существует"
   :test-str                 "немного %s, %s"})

(defn str-get [k lang & args]
  (case (keyword lang)
    :ru-RU (let [s (get dictionary k)] (apply format s args))
    (throw (UnsupportedOperationException. (str "do not support lang " lang)))))
