(ns imagelang.util.responses
  (:require [ring.util.http-response :as resp]
            [clojure.tools.logging :as log]
            [clojure.spec.alpha :as s]))

(defn e-precondition [msg & [data]]
  (log/error msg)
  (resp/precondition-failed (merge {:result :error, :message msg} data)))

(defn e-internal [e msg & [data]]
  (log/error e msg)
  (resp/internal-server-error (merge data {:result :error, :message msg, :cause e})))

(defn wrap-internal-error
  [& {:keys [fun msg data]}]
  {:pre [(s/valid? fn? fun) (s/valid? string? msg)]}
  (try (resp/ok (fun))
       (catch Exception e
         (e-internal e msg data))))