(ns imagelang.util.specs
  (:require [cemerick.url :refer (url url-encode) :as u]
            [clojure.spec.alpha :as s :refer [valid?]]
            [clojure.spec.gen.alpha :as gen]))

(defn- non-empty-string-alphanumeric
  []
  (gen/such-that #(not= "" %) (gen/string-alphanumeric)))

(defn- url-gen
  "Generator for generating URLs; note that it may generate
  http URLs on port 443 and https URLs on port 80, and only
  uses alphanumerics"
  []
  (gen/fmap
    (partial apply (comp str u/->URL))
    (gen/tuple
      (gen/elements #{"http" "https"})                      ;; protocol
      (gen/string-alphanumeric)                             ;; username
      (gen/string-alphanumeric)                             ;; password
      (gen/string-alphanumeric)                             ;; host
      (gen/choose 1 65535)                                  ;; port
      (gen/fmap #(->> %                                     ;; path
                      (interleave (repeat "/"))
                      (apply str))
                (gen/not-empty
                  (gen/vector
                    (non-empty-string-alphanumeric))))
      (gen/map                                              ;; query
        (non-empty-string-alphanumeric)
        (non-empty-string-alphanumeric)
        {:max-elements 2})
      (gen/string-alphanumeric))))                          ;; anchor

(defn str-with-len>? [l]
  (s/with-gen
    (s/and string? #(>= l (count %)))
    #(gen/fmap clojure.string/join (gen/vector (gen/char-alphanumeric) l))))

(defn g [key]
  (gen/generate (s/gen key)))

;; SPECS
(s/def ::url (s/with-gen
               (s/and string?
                      #(try
                         (u/url %)
                         (catch Throwable t false)))
               url-gen))

(comment
  (gen/generate (url-gen))
  (gen/generate (non-empty-string-alphanumeric))
  )