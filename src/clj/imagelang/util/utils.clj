(ns imagelang.util.utils
	(:require [clojure.pprint :as pprint]
						[imagelang.config :refer [env]]
						[clojure.tools.logging :as log]
						[cuerdas.core :as str]))

(defn ex-chain [^Exception e]
	(take-while some? (iterate ex-cause e)))

(def spy #(do (println "DEBUG:" %) %))

(def spy-pp #(do (pprint/pprint "DEBUG:" %) %))

(def spy-log #(do (log/debug "SPY: " %) %))