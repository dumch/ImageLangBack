(ns imagelang.util.validation
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as str]
            [clojure.spec.gen.alpha :as gen]
            [imagelang.util.resources :as res]))

(def ^:private spec-msgs (atom {}))

(defmacro defspec [name rule & [human-msg]]
  `(do (s/def ~name ~rule)
       (when ~human-msg
         (swap! spec-msgs assoc ~name ~human-msg))))

(defspec ::id
  (s/and string? #(re-matches #"^(?=.{2,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$" %))
  :login-validation)

(defspec ::pass
  (s/and string? #(re-matches #"^(?=.*\d)(?=.*[a-z]).{6,20}$" %))
  :password-validation)

(defspec ::pass_confirm
  #(= (:pass %) (:pass_confirm %))
  :pass-confirm-validation)

(defspec :unq/person
  (s/merge ::pass_confirm (s/keys :req-un [::id ::pass]))
  :unique-person-validation)

(defn- form->key-errors [form spec lang]
  (when-let [problems (::s/problems (s/explain-data spec form))]
    (let [ids (-> form keys set)]
      (->> (for [problem problems
                 :let [specs (-> problem :via)
                       msg   (res/str-get (get @spec-msgs (last specs)) lang)
                       id    (->> specs
                                  (map (comp keyword name))
                                  (filter ids)
                                  last)]]
             [id msg])
           (into {})))))

(defn- key->errors [k spec lang]
  (when-let [problems (::s/problems (s/explain-data spec k))]
    (->> (for [problem problems
               :let [specs (-> problem :via)
                     msg   (res/str-get (get @spec-msgs (last specs)) lang)
                     id    (-> spec name keyword)]]
           [id msg])
         (into {}))))

(defn registration-errors [params lang]
  (form->key-errors params :unq/person lang))

(defn change-pass-errors [pass lang]
  (key->errors pass ::pass lang))