(ns imagelang.handler-test
  (:require
    [clojure.test :refer :all]
    [imagelang.db.core :refer [*db*] :as db]
    [luminus-migrations.core :as migrations]
    [imagelang.config :refer [env]]
    [ring.mock.request :refer :all]
    [next.jdbc :as jdbc]
    [imagelang.handler :refer :all]
    [imagelang.middleware.formats :as formats]
    [muuntaja.core :as m]
    [mount.core :as mount])
  (:import (java.util Base64)))

(defn parse-json [body]
  (m/decode formats/instance "application/json" body))

(use-fixtures
  :once
  (fn [f]
    (mount/start #'imagelang.config/env
                 #'imagelang.handler/app-routes
                 #'imagelang.db.core/*db*)
    (migrations/migrate ["migrate"] (select-keys env [:database-url]))
    (f)))

(deftest test-app 
  (testing "main route"
    (let [response ((app) (request :get "/"))]
      (is (= 301 (:status response)))))

  (testing "not-found route"
    (let [response ((app) (request :get "/invalid"))]
      (is (= 404 (:status response)))))



  (testing "auth"
    (jdbc/execute! *db* ["delete from users"])

    (let [pass        "1Q12345m"
          auth-base64 (str "Basic " (.encodeToString (Base64/getEncoder) (.getBytes (str "dumch:" pass))))
          user        {:id           "dumch"
                       :pass         pass
                       :pass_confirm pass}]
      (testing "can register"
        (let [response ((app) (-> (request :post "/api/auth/register") (json-body user)))]
          (is (= 1 (-> (jdbc/execute! *db* ["select count(*) from users"]) first :count)))
          (is (= 200 (:status response)))

          (is (= "ok" (:result (m/decode-response-body response))))))

      (testing "can login"
        (let [response ((app) (-> (request :post "/api/auth/login")
                                  (header :authorization auth-base64)))]
          (is (not= nil (-> response :headers (get "Set-Cookie"))))
          (is (= 200 (:status response)))))

      (testing "can logout"
        (let [response ((app) (-> (request :post "/api/auth/logout")
                                  (header :authorization auth-base64)))]
          (is (= nil (-> response :headers (get "Set-Cookie"))))
          (is (= 200 (:status response)))))

      ;; fix test, may be provider *db* here
      #_(testing "can get words"
        (let [response ((app) (-> (request :get "/api/study/words")
                                  (header :authorization auth-base64)))]
          (is (= 200 (:status response)))
          (is (vector? (:words (m/decode-response-body response))))))

      (testing "precondition fail"
        (let [response ((app) (-> (request :post "/api/auth/register") (json-body (update user :pass subs 1))))]
          (is (= 412 (:status response)) "when passwords are not eq, should 412"))

        (let [response ((app) (-> (request :post "/api/auth/register") (json-body user)))]
          (is (= 412 (:status response)) "when user is already created, should 412")))

      (jdbc/execute! *db* ["delete from users"])

      (testing "parameter coercion error"
        (let [response ((app) (-> (request :post "/api/auth/register") (json-body (assoc user :pass 13))))]
          (is (= 400 (:status response))))))))