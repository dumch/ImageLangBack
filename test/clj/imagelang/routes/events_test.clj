(ns imagelang.routes.events-test
  (:require [imagelang.routes.events :refer :all :as events]
            [imagelang.db.core :refer [*db*] :as db]
            [imagelang.config :refer [env]]
            [imagelang.routes.words :as words]
            [imagelang.util.specs :as specs]
            [clojure.test :refer :all]
            [clojure.spec.alpha :as s :refer [valid?]]
            [clojure.spec.gen.alpha :as gen]
            [luminus-migrations.core :as migrations]
            [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [mount.core :as mount]
            [clojure.pprint :refer :all])
  (:import (java.time LocalDateTime ZoneOffset)))

(use-fixtures
  :once
  (fn [f]
    (mount/start
      #'imagelang.config/env
      #'imagelang.db.core/*db*)
    (migrations/migrate ["migrate"] (select-keys env [:database-url]))
    (f)))

(def sam-id (str Integer/MAX_VALUE))
(def bob-id (-> Integer/MAX_VALUE dec str))
(def user-ids (into [sam-id bob-id] (->> (range 10) (map str))))
(def counts {:users (count user-ids), :words 30, :repeat_idx 7})
(def repeat->intervals {0 0, 1 1, 2 2, 3 4, 4 8, 5 16, 6 32, 7 64})

(defn create-event [user-id word-id repeat-idx date]
  {:word_id           word-id
   :user_id           user-id
   :repeat_index      repeat-idx
   :interaction_count (+ repeat-idx 2)
   :event_date        date
   :meta              {:input_type  "exercise"
                       :result      "SUCCESS"
                       :action_type "SUCCESS"}})

(defn get-repeat-interval [repeat-idx]
  (or (get repeat->intervals repeat-idx) 64))

(def events-with-intervals-not-ready
  (for [user-id    user-ids
        word-id    (range (:words counts))
        repeat-idx (range 1 (inc (:repeat_idx counts)))
        :let [date (LocalDateTime/now)]]
    (create-event (str user-id) word-id repeat-idx date)))

(def events-with-all-intervals-ready
  (for [event events-with-intervals-not-ready]
    (let [count-days-to-overlap-interval (inc (get-repeat-interval (:repeat_index event)))]
      (assoc event :event_date (.minusDays (LocalDateTime/now) count-days-to-overlap-interval)))))

(def dictionary-rows
  (for [word-id (range (:words counts))]
    (assoc (gen/generate (s/gen ::words/dictionary-row)) :id word-id)))

(defn add-events-with-all-intervals-ready [ds]
  (doseq [e events-with-all-intervals-ready] (sql/insert! ds :events e)))

(defn add-events-with-intervals-not-ready [ds]
  (doseq [e events-with-intervals-not-ready] (sql/insert! ds :events e)))

(defn add-words [ds]
  (doseq [word dictionary-rows] (sql/insert! ds :dictionary word)))

(defn add-users [ds]
  (doseq [id user-ids]
    (sql/insert!
      ds :users
      {:id id :first_name id :last_name id :email (str id "@example.com") :pass (str "pass" id)})))

(defn prepare-users-and-words [ds]
  (testing "populating users"
    (add-users ds)
    (is (= (:users counts) (-> (sql/query ds ["select count(*) from users"]) first :count))))
  (testing "populating words"
    (add-words ds)
    (is (= (:words counts) (-> (sql/query ds ["select count(*) from dictionary"]) first :count)))))

(deftest test-grab-all-words-to-learn-from-events
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]

    (prepare-users-and-words ds)

    (testing "when all intervals ready, get all words for a user"
      ;; "populating events with ready intervals"
      (add-events-with-all-intervals-ready ds)
      (is (= (->> counts vals (reduce *))
             (-> (sql/query ds ["select count(*) from events"]) first :count)))
      #_(pprint (jdbc/execute! ds ["select * from events where user_id = ?" bob-id]))
      (doseq [id user-ids]
        (is (= (:words counts)
               (count (db/get-words-to-repeat
                        ds
                        {:user_id id :count (:words counts)})))))))) ;(test-grab-all-words-from-events)

(deftest test-grab-no-words-from-events
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]

    (prepare-users-and-words ds)

    (testing "when no intervals ready, get 0 words for a user"
      (testing "populating events with not ready intervals"
        (add-events-with-intervals-not-ready ds)
        (is (= (->> counts vals (reduce *))
               (-> (sql/query ds ["select count(*) from events"]) first :count))))
      (doseq [id user-ids]
        (is (= 0
               (count (db/get-words-to-repeat
                        {:user_id id :count (:words counts)})))))))) ; (test-grab-no-words-from-events)

(deftest test-grab-specific-words-from-events
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]

    (prepare-users-and-words ds)

    (testing "when only one word ready, get it for a user"

      (testing "populating events with not ready intervals"
        (add-events-with-intervals-not-ready ds)
        (is (= (->> counts vals (reduce *))
               (-> (sql/query ds ["select count(*) from events"]) first :count))))

      (let [repeat-idx (gen/generate (s/gen ::events/repeat_index))
            user-id    bob-id
            word-id    (:words counts)
            date       (.minusDays (LocalDateTime/now) (inc (get-repeat-interval repeat-idx)))
            word       (assoc (gen/generate (s/gen ::words/dictionary-row)) :id word-id)]
        (sql/insert! ds :dictionary word)
        (sql/insert! ds :events (create-event user-id word-id repeat-idx date))
        (is (= 1 (count (db/get-words-to-repeat ds {:user_id user-id})))))))) ; (test-grab-specific-words-from-events)

(deftest test-grab-words-to-repeat-and-learn
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]
    (prepare-users-and-words ds)

    (testing "when have 10 unknown (not in events) words, user should... "
      (add-events-with-all-intervals-ready ds)

      (let [new-words-count 10]
        (doseq [i (range new-words-count)
                :let [word-id (+ i (:words counts))]]
          (sql/insert! ds :dictionary (assoc (gen/generate (s/gen ::words/dictionary-row)) :id word-id)))
        (testing "get new words"
          (doseq [id user-ids]
            (is (= new-words-count (count (db/get-words-to-study ds {:user_id id}))))))

        (testing "get words to repeat"
          (doseq [id user-ids]
            (is (= (:words counts)
                   (count (db/get-words-to-repeat
                            ds
                            {:user_id id :count (:words counts)})))))))))) ;(test-grab-words-to-repeat-and-learn)

(deftest test-grab-words-for-additional-repetition
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]
    (prepare-users-and-words ds)

    (testing "for additional words to repeat, user gets words with worst history"
      (add-events-with-intervals-not-ready ds)

      (testing "get new words"
        (doseq [id [bob-id]
                :let [w-count (inc (:words counts))
                      word-id w-count
                      event   (assoc (create-event id word-id 0 (LocalDateTime/now))
                                :interaction_count 9999)
                      _       (sql/insert!
                                ds :dictionary
                                (assoc (gen/generate (s/gen ::words/dictionary-row)) :id word-id))
                      _       (sql/insert! ds :events event)

                      words   (db/get-extra-words ds {:user_id bob-id :count w-count})]]
          (is (= w-count (count words)))
          (assert (some #{word-id} (map :id words))))))))   ; (test-grab-words-for-additional-repetition)

;; events
(defn- create-correct-exercise-event [user-id word-id]
  (-> (assoc (gen/generate (s/gen :user/event))
        :event_date (LocalDateTime/now)
        :user_id user-id
        :word_id word-id)
      (assoc-in [:meta :input_type] events/EXERCISE)
      (assoc-in [:meta :result] events/SUCCESS)))

(defn- create-not-exercise-event [user-id word-id]
  (let [new-event (-> (assoc (gen/generate (s/gen :user/event))
                        :event_date (LocalDateTime/now)
                        :user_id user-id
                        :word_id word-id))]
    (assoc-in new-event [:meta :input_type] (if (= 0 (rand-int 2)) events/RESULT events/CARD))))

(defn- create-incorrect-exercise-event [user-id word-id]
  (let [new-event (-> (assoc (gen/generate (s/gen :user/event))
                        :event_date (LocalDateTime/now)
                        :user_id user-id
                        :word_id word-id))
        new-event (assoc-in new-event [:meta :input_type] events/EXERCISE)]
    (assoc-in new-event [:meta :result] events/FAIL)))

(defn- test-with-generated-events
  "ds - data source, *db*;
   generate-event-f takes 2 args: user-id and word-id;
   check-f takes 2 args: previous recent event and recent-event"
  [ds generate-event-f check-f]
  (prepare-users-and-words ds)
  (let [word-ids         (take 1 (range (:words counts)))
        user-id          bob-id
        word-id->rep-idx (into {} (map #(vector % (rand-int (:repeat_idx counts))) word-ids))
        word-id->event   (into {} (for [[word-id, repeat-idx] word-id->rep-idx
                                        :let [date-to-allow-rep (.minusDays
                                                                  (LocalDateTime/now)
                                                                  (inc (get-repeat-interval repeat-idx)))]]
                                    (vector word-id
                                            (create-event user-id word-id repeat-idx date-to-allow-rep))))
        _                (doseq [e (vals word-id->event)] (sql/insert! ds :events e))]

    (pprint (str "before: events count: " (count (db/get-recent-events ds {:user_id user-id}))))
    (doseq [word-id word-ids]
      (on-event ds user-id (generate-event-f user-id word-id)))
    (doseq [event (db/get-recent-events ds {:user_id user-id})
            :let [old-event (get word-id->event (:word_id event))]]
      (check-f old-event event))))

(deftest test-correct-exercise-events
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]
    (testing "on all exercises correct -> update intervals"
      (test-with-generated-events
        ds
        create-correct-exercise-event                       ;; generate event fn
        (fn [old-event new-event]                           ;; assert fn
          (is (= (-> old-event :repeat_index inc) (:repeat_index new-event))))))))

(deftest test-all-events-except-exercises
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]
    (testing "on all non-exercise events do not touch intervals"
      (test-with-generated-events
        ds
        create-not-exercise-event                           ;; generate event fn
        (fn [old-event new-event]                           ;; assert fn
          (is (= (-> old-event :repeat_index) (:repeat_index new-event))))))))

(deftest test-incorrect-exercises
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]
    (testing "on incorrect-exercise events decrease intervals"
      (test-with-generated-events
        ds
        create-incorrect-exercise-event                     ;; generate event fn
        (fn [old-event new-event]                           ;; assert fn
          (is (= (-> old-event :repeat_index dec) (:repeat_index new-event))))))))

(deftest test-extra-work
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]
    (testing "on extra work do not touch intervals"
      (prepare-users-and-words ds)
      (let [user-id bob-id
            limit   2
            now     (LocalDateTime/now)]

        ;; adding old event yesterday and new event today
        (doseq [word-id (take limit (range (:words counts)))
                :let [idx       (gen/generate (s/gen ::events/repeat_index))
                      old-event (create-event user-id word-id idx (.minusDays now 1))
                      new-event (create-correct-exercise-event user-id word-id)]]
          (sql/insert! ds :events old-event)
          (on-event ds user-id new-event))

        ;; adding one more event and check intervals aren't changed
        (let [user-id          user-id
              words            (db/get-extra-words ds {:user_id user-id :count limit})
              _                (prn "extra words count " (count words))
              recent-events    (db/get-recent-events ds {:user_id user-id})
              word-id->rep-idx (into {} (map #(vector (:word_id %) (:repeat_index %)) recent-events))]
          (doseq [word-id (map :id words)]
            (on-event ds user-id (create-correct-exercise-event user-id word-id)))
          (doseq [new-event (db/get-recent-events ds {:user_id user-id})
                  :let [old-rep-idx (get word-id->rep-idx (:word_id new-event))
                        new-rep-idx (:repeat_index new-event)]]
            (is (= old-rep-idx new-rep-idx))))))))

(deftest test-can-get-recent-event
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]
    (prepare-users-and-words ds)
    (testing "can get recent event event when it's happened later by one minute (test minutes are stored)"
      (let [minus-day-f (fn [event]
                          (let [date (LocalDateTime/parse (str (:event_date event)))]
                            (assoc event :event_date (.minusMinutes date 1))))
            user-id     bob-id
            events      (map minus-day-f events-with-intervals-not-ready)
            new-word-id (rand-int (:words counts))
            repeat-idx  (gen/generate (s/gen ::events/repeat_index))
            new-event   (create-event user-id new-word-id repeat-idx (LocalDateTime/now))]
        (sql/insert! ds :events new-event)
        (doseq [event events] (sql/insert! ds :events event))
        (is (= (dissoc new-event :meta)
               (dissoc (db/get-recent-event ds {:user_id user-id, :word_id new-word-id}) :id :meta)))))))

(deftest test-when-have-no-previous-events
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]
    (prepare-users-and-words ds)
    (testing "when learn a word for the first time, should have repeat_index as 1"
      (let [user-id         bob-id
            word-id         0
            now             (LocalDateTime/now)
            card-event      (assoc-in (create-not-exercise-event user-id word-id)
                                      [:meta :input_type] events/CARD)
            result          (assoc-in (create-not-exercise-event user-id word-id)
                                      [:meta :input_type] events/RESULT)
            good-exercise-e (create-correct-exercise-event user-id word-id)]
        (on-event ds user-id (assoc card-event :event_date (.minusMinutes now 2)))
        (on-event ds user-id (assoc result :event_date (.minusMinutes now 1)))
        (on-event ds user-id (assoc good-exercise-e :event_date now))
        (prn "exercises count " (jdbc/execute! ds ["select count(*) from events"]))
        (let [event (db/get-recent-event ds {:user_id user-id, :word_id word-id})]
          ;; basically it's impossible to have first event as an exercise, but...
          (is (= 1 (:repeat_index event)) "after first correct answer we should have repeat_index = 1")
          (is (= 3 (:interaction_count event)) "we should have 3 interaction count"))))))

(defn- gen-events
  "generate all the events with all input-, action- and result- types given word-ids and dates"
  [event word-ids dates]
  (for [word-id word-ids
        date    dates
        a-type  [events/WORD_TR events/TR_WORD events/PROCEED events/WRITE]
        i-type  [events/CARD events/EXERCISE events/RESULT]
        r-type  [SUCCESS NEXT PREVIOUS]]
    (assoc event
      :meta {:input_type i-type, :action_type a-type, :result r-type},
      :event_date date, :word_id word-id)))

(deftest test-get-learned-today-count
  (jdbc/with-transaction
    [ds *db* {:rollback-only true}]
    (prepare-users-and-words ds)
    (let [split-idx       (-> counts :words (/ 2))
          event           {:user_id           bob-id
                           :interaction_count (specs/g ::events/interaction_count)
                           :repeat_index      (specs/g ::events/repeat_index)}
          today           (LocalDateTime/now)
          insert-events-f (fn [word-ids dates]
                            (doseq [e (gen-events event word-ids dates)]
                              (sql/insert! ds :events e)))]
      ;; add all events except today events
      (testing "when each word has events during the last 10 days, get sure those words are not today learned"
        (insert-events-f (range split-idx)
                         ;; last 10 days including today
                         (->> (range 10) (map #(.minusDays ^LocalDateTime today ^long %))))
        (prn "learned before adding new: " (:count (db/get-learned-today-count ds {:user_id bob-id})))
        (is (= 0 (:count (db/get-learned-today-count ds {:user_id bob-id})))))

      (testing "only new successful words from today should be considered as learned today"
        (insert-events-f (range split-idx (:words counts)) [today])
        (prn "learned after adding new: " (:count (db/get-learned-today-count ds {:user_id bob-id})))
        (is (= split-idx (:count (db/get-learned-today-count ds {:user_id bob-id}))))))))

(defn- time->millis [time]
  (.toEpochMilli (.toInstant (.atZone time ZoneOffset/UTC))))

(deftest test-on-events-batch
  (let [d-now    (LocalDateTime/now)
        dates    (for [n (range 4)] (.minusMinutes d-now n))
        previous (assoc (specs/g ::events/event) :event_date (.minusMinutes d-now 4))
        events   (gen-events (specs/g :user/event) [1] dates)
        events+  (#'events/enrich-events bob-id previous events)]
    (testing "enrich-events"
      (is (apply < (map :interaction_count events+))
          "check interaction count change")
      (is (apply <= (->> events+ (map :event_date) (map time->millis)))
          "check events are sorted in order"))

    (testing "on-events"
      (jdbc/with-transaction
        [ds *db* {:rollback-only true}]
        (prepare-users-and-words ds)
        (on-events ds bob-id events)
        (is (= (count events+)
               (:count (first (next.jdbc/execute! ds ["select count(*) from events"])))))))))

(comment
  ;for dataset:
  ; events 4284000
  ; words 3000
  ; users 102
  ; intervals 14
  ;we have:

  ;; get new words
  (def get-words-1 " -- {:QUERY PLAN Execution Time: 8 ms} -- with multi-column index
  SELECT * FROM   dictionary d
  WHERE  NOT EXISTS (
     SELECT DISTINCT word_id FROM   events
     WHERE  user_id = ? AND word_id = d.id
  ) LIMIT 10; ")

  (def get-words-2 " -- {:QUERY PLAN Execution Time: 19.788 ms}
  SELECT * FROM   dictionary
  WHERE  id NOT IN (
     SELECT DISTINCT word_id FROM   events
     WHERE user_id = ?
  ) LIMIT 10; ")

  (def get-words-3 " -- {:QUERY PLAN Execution Time: 19.492 ms}
  SELECT d.* FROM   dictionary d
  LEFT   JOIN events e ON d.id = e.word_id AND e.user_id = ?
  WHERE  e.word_id IS NULL LIMIT 10; ")


  ; get extra words
  (def get-extra-words-1 " -- {:QUERY PLAN Execution Time: 55-74 ms }

  LIMIT 50 ")

  (def get-extra-words-2 " -- {:QUERY PLAN Execution Time: 54-66 ms }
  WITH T AS(
             SELECT *, ROW_NUMBER() OVER (
                                           PARTITION BY word_id
                                           ORDER     BY event_date DESC
                                                     ) AS rn
                    FROM events WHERE user_id = ?
                    )
  SELECT dictionary.* FROM dictionary
 JOIN T ON dictionary.id = T.word_id AND user_id = ? AND T.rn = 1
  ORDER BY (interaction_count - repeat_index) DESC
  LIMIT 50 ")

  (def get-extra-words-3 "  -- {:QUERY PLAN Execution Time: 69-71 ms }
   WITH T AS(
      SELECT *, ROW_NUMBER() OVER (
                     PARTITION BY word_id
                     ORDER     BY event_date DESC
                     ) AS rn
      FROM events WHERE user_id = ?
      ORDER BY (interaction_count - repeat_index) DESC
      )
   SELECT dictionary.* FROM dictionary
   LEFT JOIN T ON dictionary.id = T.word_id AND T.rn = 1
   LIMIT 50 ")
  )
