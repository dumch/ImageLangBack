(ns imagelang.routes.words-test
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as s :refer [valid?]]
            [clojure.spec.gen.alpha :as gen]
            [imagelang.routes.words :as words]
            [imagelang.db.core :as db :refer [*db*]]
            [imagelang.util.utils :refer [spy]]
            [imagelang.util.specs :as specs]
            [clojure.pprint :as pprint]))

(defn- wrap-words-validation [words]
  (doseq [word words] (assert (s/valid? :exercise/word word) "getting invalid word"))
  words)

(defn- get-words [to-study, to-repeat, learned-today-count, to-learn-count]
  (let [words-gotten (#'words/get-words-to-learn-and-repeat
                       "user"
                       to-learn-count
                       (fn [user-info] to-repeat)
                       (fn [user-info] {:count learned-today-count})
                       (fn [{u :user_id c :count}] (take c to-study)))]
    (wrap-words-validation words-gotten)))

(defn- check-url
  [word key & keys]
  (when (some? key)
    (let [url (get word key)]
      (assert (s/valid? ::specs/url url) (str "bad image url " url)))
    (apply check-url word (first keys) (seq (rest keys)))))

(deftest get-words-test
  (let [size            10
        words           (gen/sample (s/gen ::words/dictionary-row) size)
        words-to-repeat (gen/sample (s/gen ::words/dictionary-row) size)]

    (is (= size (count (get-words words [] 0 size)))
        (str "when have nothing to repeat and leaned 0, should get " size " words to learn"))

    (is (= (* 2 size) (count (get-words words words-to-repeat 0 size)))
        (str "when have learned 0, should get " size " words to learn and all words to repeat"))

    (is (= size (count (get-words words words-to-repeat size size)))
        (str "when have learned " size ", should get 0 words to learn and all words to repeat"))

    (testing "test urls"
      (doall (->> (get-words words words-to-repeat size size)
                  (map (fn [w] (check-url w :image_file :sound_file :meaning_sound_file :example_sound_file))))))

    (testing "test +dictionary mapper"
      (let [dic-size       10
            dic-words      (gen/sample (s/gen ::words/dictionary-row) (* dic-size 4))
            words-with-dic (->> (get-words words words-to-repeat size size)
                                (#'words/+dictionary dic-size (fn [{count :count}]
                                                                (take count dic-words))))
            dict           (:dictionary words-with-dic)
            dict-native    (:dictionary_native words-with-dic)]
        (assert (s/valid? (s/coll-of string?) dict) "dictionary should be collection of strings")
        (assert (s/valid? (s/coll-of string?) dict-native) "native dictionary should be collection of strings")
        (is (= dic-size (count dict)))
        (is (= dic-size (count dict-native)))))))
